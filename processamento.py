import os
import fnmatch
import rasterio
import pandas as pd
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
from pathlib import Path
from tensorflow import keras
from rasterio.plot import show
from keras.layers import Dense
from google.colab import drive
from shapely.geometry import Point
from keras.models import Sequential
from eobox.raster import extraction
from sklearn.model_selection import train_test_split

# Commented out IPython magic to ensure Python compatibility.
!mkdir img

# Commented out IPython magic to ensure Python compatibility.
!mkdir sentinel1_assets
# %cd /content/img/sentinel1_assets
!wget http://www.dpi.inpe.br/obt/agricultural-database/lem/dados/cenas/Sentinel1/20180619_S1A/clip_20180619T083553_Sigma0_VH_db.tif
!wget http://www.dpi.inpe.br/obt/agricultural-database/lem/dados/cenas/Sentinel1/20180619_S1A/clip_20180619T083553_Sigma0_VV_db.tif

# Commented out IPython magic to ensure Python compatibility.
!mkdir /content/img/sentinel2_assets
# %cd /content/img/sentinel2_assets
!wget http://www.dpi.inpe.br/obt/agricultural-database/lem/dados/cenas/Sentinel2/Sentinel-2A_Images/S2A_MSIL2A_20180629.zip

!unzip ./S2A_MSIL2A_20180629.zip

drive.mount('/content/drive')

!unzip /content/drive/"My Drive"/FATEC/LEMS.zip -d /content/

!ls /content/LEMS/

shp1_dir = "/content/LEMS/LEM.shp"
shp2_dir = "/content/LEMS/shape.shp"
shp1_file = gpd.read_file(shp1_dir)
shp2_file = gpd.read_file(shp2_dir)

shp1_file.plot()

shp2_file.plot()

!ls /content/img/sentinel2_assets/

s2_img1 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_B02_10m.jp2"
raster = rasterio.open(s2_img1)
plt.imshow(raster.read(1))

dir_ = "/content/img/sentinel2_assets/"

sub_folders = []
for path, subdirs, files in os.walk(dir_):
    for file in files:
        sub_folders.append(os.path.join(path, file))

# Commented out IPython magic to ensure Python compatibility.
# %time
s2_img2 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_B02_10m.jp2"
s2_img3 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_B03_10m.jp2"
s2_img4 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_B04_10m.jp2"
s2_img5 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_B08_10m.jp2"
s2_img6 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_TCI_10m.jp2"
s2_img7 = "/content/img/sentinel2_assets/S2A_MSIL2A_20180629T132231_N0206_R038_T23LLG_20180629T164308.SAFE/GRANULE/L2A_T23LLG_A015765_20180629T132233/IMG_DATA/R10m/T23LLG_20180629T132231_WVP_10m.jp2"
extraction.extract(src_vector= shp1_dir,
                   burn_attribute= "ID",
                   src_raster= [s2_img2,s2_img3,s2_img4,s2_img5,s2_img6,s2_img7],
                   dst_names= ["img2", "img3", "img4", "img5", "img6", "img7"],
                   dst_dir= "/content/dataset",
                   n_jobs= -1)

result_tif = "/content/dataset/burn_attribute_rasterized_ID.tif"
result = rasterio.open(result_tif)
plt.imshow(result.read(1))

extractedFile = extraction.load_extracted("/content/dataset", patterns=["aux_*.npy", "im*??.npy"])
geometry = [Point(xy) for xy in zip(extractedFile.aux_coord_x, extractedFile.aux_coord_y)]
gdf_gen = gpd.GeoDataFrame(
    extractedFile,
    crs = {'init': 'epsg:32723'},
    geometry = geometry
)
gdf_gen.head()

# Variável criada para armazenar os dados analisados a partir das imagens do Satélite

gdf_gen["ndvi"] = (gdf_gen["img7"] - gdf_gen["img3"])/(gdf_gen["img7"] + gdf_gen["img3"])
gdf_gen.plot(column="ndvi",figsize=(10,10))

!unzip /content/drive/"My Drive"/FATEC/dataset_in.zip -d /content/dataset/

meta_dados = pd.read_csv("/content/dataset/dataset_in.csv")
meta_dados.head()

ndvi_temp = meta_dados['in_ndvi']
meta_dados = meta_dados/meta_dados.max()
meta_dados['in_ndvi'] = ndvi_temp
meta_dados = meta_dados.drop(["Unnamed: 0","x","y"],axis=1)
meta_dados.loc[meta_dados["result"] > 0, 'result'] = 1

arr_out = []
result_column= meta_dados["result"]
for i in range(3348900):
  if(result_column[i] == 1):
    arr_out.append([0,1])
  else:
    arr_out.append([1,0])
np.array(arr_out)

result_img = []
for i in range(0,1830):
  col = []
  for j in range(0,1830):
    col.append(meta_dados['result'][j+i*1830])
  result_img.append(col)
result_img = np.array(result_img)

plt.figure(figsize = (8,8))
plt.imshow(result_img)

y = np.array(arr_out)
x = meta_dados.drop(columns="result",axis=0).to_numpy()
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.3, random_state=1)

model = Sequential()
input_shape = x_train[0].shape
model.add(Dense(5,activation='relu',input_shape=input_shape))
model.add(Dense(5,activation='relu'))
model.add(Dense(2,activation='relu'))
model.add(Dense(2,activation='sigmoid'))

model.summary()

model.compile(optimizer="adam", loss="binary_crossentropy",metrics=["acc"])

model.fit(x_train,y_train, epochs=100, batch_size=50000, shuffle=True)

test_loss, test_acc = model.evaluate(x_test,  y_test, verbose=2)

print('\nPorcentagem de Sucesso no reconhecimento:', test_acc)

preds = model.predict(x,batch_size=50000)

prev_img = []
def eps(n):
  return n > 0.042
for i in range(0,1830):
  prev_img.append([])
  for j in range(0,1830):
    prev_img[i].append(1 if eps(preds[j+i*1830][1]) else 0)
prev_img = np.array(prev_img)

plt.figure(figsize = (8,8))
plt.imshow(img_pred)clearclw