# coding: utf-8

from flask import Flask, request, jsonify, Blueprint
from flask_cors import CORS
from flask_restplus import Api, Resource, fields
import json
import time
import datetime
import requests
import pyowm
from pyowm.utils.geo import Polygon as GeoPolygon

from det import *

import cloudinary
import cloudinary.uploader

cloudinary.config(
    cloud_name='douglassouzag',
    api_key='223983572231185',
    api_secret='LEd3SNkblBpQrdGSIHxP4pJ0w0c')


API_KEYS = [
    'dea1026649d5f0b48dff8f3e0bd7176b'
]
API_ID = API_KEYS[0]
owm = pyowm.OWM(API_ID)
agro = owm.agro_manager()


app = Flask(__name__)
app.config['SECRET_KEY'] = 'CHAVESECRETAFATEC2020PI6SEMGR5'
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
blueprint = Blueprint('api', __name__, url_prefix='/api')
api = Api(
    blueprint,
    doc='/doc/',
    version = "1.0", 
	title = "Visiona", 
	description = "Descrição aqui"
)
app.register_blueprint(blueprint)

area = api.namespace('area', description='Áreas')
CORS(app,supports_credentials=True)


#MODELS

modelGeoJSON = api.model('GeoJSON',{'geojson': fields.Raw(required = True)})
modelProc = api.model('Processamento',{'url': fields.String()})

def uploadImg(file):
    # file = request.files['picture']
    try:
        upload_data = cloudinary.uploader.upload(file)
        link = upload_data['url']
    except:
        link =''
        pass
    return link

def retornarLinkImagens(id,data_inicio,data_fim):
        
    array_produtos = []
    
    dt_inicio = time.mktime(datetime.datetime.strptime(data_inicio, "%d/%m/%Y").timetuple())
    dt_fim = time.mktime(datetime.datetime.strptime(data_fim, "%d/%m/%Y").timetuple())

    url = 'http://api.agromonitoring.com/agro/1.0/image/search'
    params = {
        "start": dt_inicio,
        "end": dt_fim,
        "polyid": id,
        "appid": API_ID
    }
    try:
        resp = requests.get(url=url,params=params)
        data = json.loads(resp.text)
    except Exception as erro:
        print(str(erro))
    for produto in data:
        # if 'Sentinel-2' not in produto['type']:continue           
        array_produtos.append(produto['image']['truecolor'])

    return array_produtos


@area.route('/processar')
class processarImagensArea(Resource):
    @area.expect(modelProc)
    def post(self):
        url = area.payload['url']

        print(url)
        
        arrayUploads =[]

        identificarTalhoes(url)
        arrayUploads.append(uploadImg('./out.png'))
        
        return {'imagens':arrayUploads}

@area.route('/download')
class baixarImagensArea(Resource):
    @area.expect(modelGeoJSON)
    def post(self):
        geo = area.payload['geojson']

        geo = json.loads(geo)

        coords = GeoPolygon(geo['features'][0]['geometry']['coordinates'])

        poligono = agro.create_polygon(coords, 'DEFAULT')


        array = retornarLinkImagens(poligono.id,'01/01/2020','01/04/2020')
        arrayUploads = []
        for imagem in array:
            baixarImagemPNG(imagem)
            arrayUploads.append(uploadImg('./in.png'))

        return {'imagens':arrayUploads}

if __name__ == '__main__':
    app.config['JSON_AS_ASCII'] = False
    app.run(debug=True,port=5000)