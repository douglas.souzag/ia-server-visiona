[![Python](https://img.shields.io/badge/python-v3.8-<COLOR>.svg)](https://shields.io/)
[![Flask](https://img.shields.io/badge/flask-v1.1.1-<COLOR>.svg)](https://shields.io/)
[![Tensorflow](https://img.shields.io/badge/tensorflow-v2.2.0-red.svg)](https://shields.io/)
[![Cloudinary](https://img.shields.io/badge/cloudinary-v1.21.1-green.svg)](https://shields.io/)
[![Keras](https://img.shields.io/badge/keras-v2.4.3-blue.svg)](https://shields.io/)
[![Numpy](https://img.shields.io/badge/numpy-v1.19.0-brown.svg)](https://shields.io/)
[![Matplotlib](https://img.shields.io/badge/matplotlib-v3.2.2-yellow.svg)](https://shields.io/)
[![Opencv](https://img.shields.io/badge/opencv_python-v4.2.0.34-purple.svg)](https://shields.io/)
[![Geopandas](https://img.shields.io/badge/geopandas-v0.7.0-purple.svg)](https://shields.io/)
[![Pandas](https://img.shields.io/badge/pandas-v1.0.1-purple.svg)](https://shields.io/)

# Servidor de processamento

## Instalaçao das dependencias
```
pip install -r requirements.txt
```

### Compilando e subindo versão de desenvolvimento
```
python app.py
```